#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <stdio.h>
#include <math.h>
#include <tf/transform_datatypes.h>
#include "turtlebot_twist/RotateAngle.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/Empty.h"
#define follow 1
#define parallel_stop 2
#define docking 3
int number_of_tags_kinect=0;
int number_of_tags=0;
int select_id=13, current_tag=0, search_id=0;
bool search_new_id=true;
float Et=-1.0;
float Etx=-1.0;
float Ef=-1.0;
float tag_radius=0.4;
tf::Vector3 tag_target_point(0.0,0.0,0.0);    //target_from tag frame of reference
tf::Vector3 target_point(0.0,0.0,0.0);
bool new_message=false;
bool destination_reach=false;
int state=1;

float linear_vel=0;
float angular_vel=0;
bool tag_found=false;
bool refining=false;
float Error=100;
float max_linear_vel=-100;
bool odometry_flag=false;
float x_odometry=0;
float current_x_odometry=0;
void UsbCamArPoseCallback(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& usb_arPoses)
{
number_of_tags=usb_arPoses->markers.size();
//std::cout<<"the number of tags detected are: "<<number_of_tags<<std::endl;

      ROS_INFO("Error: [%f]", Error);

//if (number_of_tags) tag_found=true;
for(int i=0;i<number_of_tags;i++)

    {
    
     if(usb_arPoses->markers[i].id==select_id)
        {
           Error= usb_arPoses->markers[i].pose.pose.position.x;
           tag_found=true;
        }


   }



}



void get_current_vel(const nav_msgs::Odometry::ConstPtr& msg){

    //ROS_INFO("Vel-> Linear: [%f], Angular: [%f]", msg->twist.twist.linear.x,msg->twist.twist.angular.z);
    linear_vel=msg->twist.twist.linear.x;
    angular_vel=msg->twist.twist.angular.z;
    x_odometry=msg->pose.pose.position.x;
}








void KinectArPoseCallback(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& arPoses)
{
    float Ez,Ex,dest;
number_of_tags_kinect=arPoses->markers.size();
//std::cout<<"the number of tags detected are: "<<number_of_tags<<std::endl;


for(int i=0;i<number_of_tags_kinect;i++)

    {

        if(arPoses->markers[i].id==select_id)
        {
        tf::Quaternion q(arPoses->markers[i].pose.pose.orientation.x,arPoses->markers[i].pose.pose.orientation.y,arPoses->markers[i].pose.pose.orientation.z,arPoses->markers[i].pose.pose.orientation.w);

        tf::Vector3 tag_translation(arPoses->markers[i].pose.pose.position.x, arPoses->markers[i].pose.pose.position.y, arPoses->markers[i].pose.pose.position.z);

        //tf::Matrix3x3 m(q);
        tf::Transform tag_transformation_matrix(q,tag_translation);
        target_point=tag_transformation_matrix.operator()(tag_target_point);

  //      std::cout<<"target point(x,y,z): ("<< target_point.x()
   //         <<"," << target_point.y() << "," << target_point.z() <<")"<<std::endl;
    //    std::cout<<"distance to point: "<<target_point.length() <<std::endl;

        Et=atan2f(target_point.x(),target_point.z());
        Etx=hypotf(target_point.z(),target_point.x());
        Ef=atan2f(tag_translation.x(),tag_translation.z());



      //  std::cout<<"El error es (theta, x): ("<<Et<<","<<Etx<<")"<<std::endl;
      //  std::cout<<"El error final (theta): ("<<Ef<<")"<<std::endl;

        new_message=true;


        //if(Etx<0.05)
        //{
         //   Etx=0.0;
        //}
        if((Et<0.15 && Et>-0.15 && Etx<0.05+tag_radius)||destination_reach)
        {
            Et=0;
            Etx=0;
       // std::cout<<"Error set to 0"<<std::endl;

        }



        //double roll, pitch, yaw;
        //m.getRPY(roll, pitch, yaw);
        //std::cout << "Roll: " << roll << ", Pitch: " << pitch << ", Yaw: " << yaw << std::endl;





        }
    }

}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "turtlePoseController_node");
    ros::NodeHandle n;

    ros::Subscriber getVel_sub=n.subscribe("odom",100,get_current_vel);

    //register to get the ar pose
    ros::Subscriber kinect_ArPose_sub=n.subscribe("/kinect_cam/ar_pose_marker",1,KinectArPoseCallback);
    ros::Subscriber usb_cam_ArPose_sub=n.subscribe("/usb_cam/ar_pose_marker",1,UsbCamArPoseCallback);
    ros::Publisher Twist_pub=n.advertise<geometry_msgs::Twist>("/cmd_vel_mux/input/switch", 1);//"/mobile_base/commands/velocity"
    ros::ServiceClient turtlebot_rotation_client=n.serviceClient<turtlebot_twist::RotateAngle>("rotate_turtlebot_srv");


ros::Publisher reset_odometry_pub=n.advertise<std_msgs::Empty>( "/mobile_base/commands/reset_odometry",100);

    geometry_msgs::Twist CmdVel;
    geometry_msgs::Twist CmdVel2;
    std_msgs::Empty reset_odometry_msg;
    ros::Rate loop_rate(30);

    while(ros::ok() && n.ok())
    {




if(state==follow)
{
ROS_INFO("follow state");

       if(abs(x_odometry)<0.2)
       { 
       ROS_INFO("ignoring tag less than 20 cm current odometry %f", x_odometry);
//	odometry_flag=false;
       }

	else if(tag_found)
	{
        ROS_INFO("tag_found");
	state=parallel_stop;
	}
	else

	{
         ROS_INFO("Searching for tag");
	}


}
else if(state==parallel_stop)
{
ROS_INFO("parallel_stop state");

//ALI code Begins

       /* if(tag_found && !refining){
            CmdVel2.linear.x=0.1;//linear_vel-0.1;
            CmdVel2.angular.z=0;
            Twist_pub.publish(CmdVel2);
        }*/
        if(Error<0.5){refining=true;
                         if(max_linear_vel==-100) max_linear_vel=linear_vel;
                         if (max_linear_vel<0) max_linear_vel=0;
	//		std::cout<<"inside error less than 0.5"<<std::endl;
                     }

        if(refining)
            {

	    //std::cout<<"inside error less than 0.588888888888888888888888"<<std::endl;
   
          //  std::cout<<"first alignment"<<std::endl;
            if (max_linear_vel>Error*0.5)   CmdVel2.linear.x=+Error*0.5;
            else CmdVel2.linear.x=max_linear_vel;
            CmdVel2.angular.z=0;//-0.5*Ef;
            Twist_pub.publish(CmdVel2);

          //    ROS_INFO("max linear: [%f]", max_linear_vel);
             if(abs(Error)<0.05){
		Error=100;
		max_linear_vel=-100;
		refining=false;
		tag_found=false;
                // Rotating to face the tag
                turtlebot_twist::RotateAngle rotation_angle_srv;
                rotation_angle_srv.request.angle=-1.57;

                if(turtlebot_rotation_client.call(rotation_angle_srv))
	       	 {
		  ROS_INFO("rotating");
		 }		
	        else
		 {
		  ROS_INFO("Failed to call the service");
		 }	

		state=docking;
                ros::service::waitForService ("rotate_turtlebot_srv", 100);

		}   
            }

//ALI code ends


}

else if(state==docking)

{
ROS_INFO("docking state");

	
        if(new_message)
        { std::cout<<"new message recieved"<<std::endl;
            if(Etx==0 && Et==0)
            {
                std::cout<<"final alignment"<<std::endl;
            CmdVel2.linear.x=0;
            CmdVel2.angular.z=-0.5*Ef;
            Twist_pub.publish(CmdVel2);
            destination_reach=true;


            turtlebot_twist::RotateAngle rotation_angle_srv;
            rotation_angle_srv.request.angle=1.57;

            if(turtlebot_rotation_client.call(rotation_angle_srv))
		{
		ROS_INFO("rotating");
		}		
	   else
		{
		ROS_INFO("Failed to call the service");
		}	
                ros::service::waitForService ("rotate_turtlebot_srv", 100);

            state=follow;
	    Etx=-1.0;
	    Et=-1.0;
	    destination_reach=false;
	    odometry_flag=true;
	   //current_x_odometry=x_odometry;
                 for(int i=0; i<10;i++)
	         {   
                 reset_odometry_pub.publish(reset_odometry_msg);
                 }
            }
            else
            {
            CmdVel.linear.x=0.2*(Etx-tag_radius);
            CmdVel.angular.z=-0.5*Et;
            Twist_pub.publish(CmdVel);

            }
            new_message=false;
        }

}

     ros::spinOnce();

   loop_rate.sleep();

    }







}

