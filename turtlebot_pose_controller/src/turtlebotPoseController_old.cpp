#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <ar_track_alvar_msgs/AlvarMarkers.h>
#include <stdio.h>
#include <math.h>
#include <tf/transform_datatypes.h>
int number_of_tags=0;
int select_id=11, current_tag=0, search_id=0;
bool search_new_id=true;
float Et=-1.0;
float Etx=-1.0;
float Ef=-1.0;
float tag_radius=0.6;
tf::Vector3 tag_target_point(0.0,0.0,0.0);    //target_from tag frame of reference
tf::Vector3 target_point(0.0,0.0,0.0);
bool new_message=false;
bool destination_reach=false;

void ArPoseCallback(const ar_track_alvar_msgs::AlvarMarkers::ConstPtr& arPoses)
{
    float Ez,Ex,dest;
number_of_tags=arPoses->markers.size();
std::cout<<"the number of tags detected are: "<<number_of_tags<<std::endl;


for(int i=0;i<number_of_tags;i++)

    {

        if(arPoses->markers[i].id==select_id)
        {
        tf::Quaternion q(arPoses->markers[i].pose.pose.orientation.x,arPoses->markers[i].pose.pose.orientation.y,arPoses->markers[i].pose.pose.orientation.z,arPoses->markers[i].pose.pose.orientation.w);

        tf::Vector3 tag_translation(arPoses->markers[i].pose.pose.position.x, arPoses->markers[i].pose.pose.position.y, arPoses->markers[i].pose.pose.position.z);

        //tf::Matrix3x3 m(q);
        tf::Transform tag_transformation_matrix(q,tag_translation);
        target_point=tag_transformation_matrix.operator()(tag_target_point);

        std::cout<<"target point(x,y,z): ("<< target_point.x()
            <<"," << target_point.y() << "," << target_point.z() <<")"<<std::endl;
        std::cout<<"distance to point: "<<target_point.length() <<std::endl;

        Et=atan2f(target_point.x(),target_point.z());
        Etx=hypotf(target_point.z(),target_point.x());
        Ef=atan2f(tag_translation.x(),tag_translation.z());



        std::cout<<"El error es (theta, x): ("<<Et<<","<<Etx<<")"<<std::endl;
        std::cout<<"El error final (theta): ("<<Ef<<")"<<std::endl;

        new_message=true;


        //if(Etx<0.05)
        //{
         //   Etx=0.0;
        //}
        if((Et<0.15 && Et>-0.15 && Etx<0.05+tag_radius)||destination_reach)
        {
            Et=0;
            Etx=0;
        std::cout<<"Error set to 0"<<std::endl;

        }



        //double roll, pitch, yaw;
        //m.getRPY(roll, pitch, yaw);
        //std::cout << "Roll: " << roll << ", Pitch: " << pitch << ", Yaw: " << yaw << std::endl;





        }
        /*
        std::cout<<"id: "<<arPoses->markers[i].id <<std::endl;
        select_id=arPoses->markers[i].id;

    //arPoses->markers[i].pose.pose.position.x;
    if(select_id==13 || select_id==15)
        {
        Ez=arPoses->markers[i].pose.pose.position.z-0.6;//-desire_position[2];
        Ex=arPoses->markers[i].pose.pose.position.x;//-desire_position[0];
        Et=atan2f(Ex,Ez);
        Etx=hypotf(Ez,Ex);
        std::cout<<"El error es (theta, x): ("<<Et<<","<<Etx<<")"<<std::endl;


    if(Etx<0.05 && Et<0.01 && Et>-0.01)
        {

            if(select_id==13)
            {
            search_id=15;
            }
            else if(select_id==15)
            {
            search_id=13;
            }

        search_new_id=true;

        }


        if(Etx<0.05)
        {
            Etx=0.0;
        }
        if(Et<0.01 && Et>-0.01)
        {
            Et=0;

        }




        }
    else
    {
    Et=0;
    Etx=0;
    }
*/
    }

}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "turtlePoseController_node");
    ros::NodeHandle n;

    //register to get the ar pose
    ros::Subscriber ArPose_sub=n.subscribe("/ar_pose_marker",1,ArPoseCallback);
    ros::Publisher Twist_pub=n.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity", 1);
    geometry_msgs::Twist CmdVel;
    geometry_msgs::Twist CmdVel2;
    ros::Rate loop_rate(20);

    while(ros::ok() && n.ok())
    {

        ros::spinOnce();


        if(new_message)
        {
            if(Etx==0 && Et==0)
            {
                std::cout<<"final alignment"<<std::endl;
            CmdVel2.linear.x=0;
            CmdVel2.angular.z=-0.5*Ef;
            Twist_pub.publish(CmdVel2);
            destination_reach=true;
            }
            else
            {
            CmdVel.linear.x=0.2*(Etx-tag_radius);
            CmdVel.angular.z=-0.5*Et;
            Twist_pub.publish(CmdVel);

            }
            new_message=false;
        }
/*
    if(search_new_id==true)
    {
//wonder
        if(search_id=13) //current_id
        {
        CmdVel.linear.x =0.1*Etx;
        CmdVel.angular.z=-0.2*Et;
        Twist_pub.publish(CmdVel);
        }


    }

*/

//}
/*
if(Etx<3 && Etx>-3)
{
CmdVel.linear.x =0.1*Etx;
CmdVel.angular.z=-0.2*Et;
Twist_pub.publish(CmdVel);
}*/
    loop_rate.sleep();

    }







}

