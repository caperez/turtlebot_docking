#!/usr/bin/env python

""" odom_out_and_back.py - Version 0.1 2012-03-24
    adopted from:
    http://nullege.com/codes/show/src%40r%40b%40rbx1-HEAD%40rbx1_nav%40nodes%40odom_out_and_back.py/27/geometry_msgs.msg.Twist/python

    A basic demo of using the /odom topic to move a robot a given distance
    or rotate through a given angle.

    Created for the Pi Robot Project: http://www.pirobot.org
    Copyright (c) 2012 Patrick Goebel.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.5

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details at:

    http://www.gnu.org/licenses/gpl.html

    !!!!!!!!!!! Turtlebot calibration (for odom)
"""

from turtlebot_twist.srv import *
import rospy
from geometry_msgs.msg import Twist, Point, Quaternion
import tf
import PyKDL
from math import radians, copysign, sqrt, pow, pi
from std_msgs.msg import Float32
from nav_msgs.msg import Odometry

''' Parameter '''
# Set the odom frame
odom_frame = '/odom'
base_frame = '/base_footprint'
rate = 20 # ROS rate
angular_speed = 0.7
angular_tolerance = radians(2.5)
published_topic = '/mobile_base/commands/velocity'
rospy.init_node('rotate_turtlebot_srv_node')
cmd_vel = rospy.Publisher(published_topic, Twist, queue_size=10)
tf_listener = tf.TransformListener() # Initialize the tf listener
r = rospy.Rate(rate)

def control_angle():
    s = rospy.Service('rotate_turtlebot_srv', RotateAngle, input_angle) #change to custom topic

    rospy.spin()


def get_odom():
    # Get the current transform between the odom and base frames
    try:
        (trans, rot)  = tf_listener.lookupTransform(odom_frame, base_frame, rospy.Time(0))
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("TF Exception")
        return

    return (Point(*trans), quat_to_angle(Quaternion(*rot)))

def input_angle(goal_angle):
    # Initialize the position variable as a Point type
    # position = Point()

    # get current orientation
    (position, rotation) = get_odom()
    rot_start = normalize_angle(rotation)

    print('starting orientation:', rot_start)

    move_cmd = Twist()
    cmd_vel.publish(move_cmd)
    rospy.sleep(1)

    # Track the last angle measured
    last_angle = rotation

    # Track how far we have turned
    turn_angle = 0

    goal = goal_angle.angle
    if goal > 0:
        # Set the movement command to a rotation
        move_cmd.angular.z = -angular_speed
    else:
        # Set the movement command to a rotation
        move_cmd.angular.z = angular_speed


    print('Goal:',goal)
    counter = 0

    while abs(turn_angle + angular_tolerance) < abs(goal):
                # Publish the Twist message and sleep 1 cycle
        cmd_vel.publish(move_cmd)
        #r.sleep()

        # Get the current rotation
        (position, rotation) = get_odom()

        # Compute the amount of rotation since the last loop
        delta_angle = normalize_angle(rotation - last_angle)

        # Add to the running total
        turn_angle += delta_angle
        last_angle = rotation

    # Stop the robot before the next leg
    move_cmd = Twist()
    cmd_vel.publish(move_cmd)
    rospy.sleep(1)

    # response to client
    return True


def quat_to_angle(quat):
    rot = PyKDL.Rotation.Quaternion(quat.x, quat.y, quat.z, quat.w)
    return rot.GetRPY()[2]

def normalize_angle(angle):
    res = angle
    while res > pi:
        res -= 2.0 * pi
    while res < -pi:
        res += 2.0 * pi
    return res

def get_delta(start, current):
    delta = start - current
    # if abs(delta) > pi:
    #    delta = 2 * pi - delta

    return delta


if __name__ == '__main__':
    # Find out if the robot uses /base_link or /base_footprint
    try:
        tf_listener.waitForTransform(odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))
        base_frame = '/base_footprint'
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        try:
            tf_listener.waitForTransform(odom_frame, '/base_link', rospy.Time(), rospy.Duration(1.0))
            base_frame = '/base_link'
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo("Cannot find transform between /odom and /base_link or /base_footprint")
            rospy.signal_shutdown("tf Exception")

    control_angle()
