#!/usr/bin/env python

""" odom_out_and_back.py - Version 0.1 2012-03-24
    adopted from:
    http://nullege.com/codes/show/src%40r%40b%40rbx1-HEAD%40rbx1_nav%40nodes%40odom_out_and_back.py/27/geometry_msgs.msg.Twist/python

    A basic demo of using the /odom topic to move a robot a given distance
    or rotate through a given angle.

    Created for the Pi Robot Project: http://www.pirobot.org
    Copyright (c) 2012 Patrick Goebel.  All rights reserved.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.5

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details at:

    http://www.gnu.org/licenses/gpl.html

"""

# import roslib; roslib.load_manifest('rbx1_nav')
import rospy
from geometry_msgs.msg import Twist, Point, Quaternion
import tf
import PyKDL
from math import radians, copysign, sqrt, pow, pi
from std_msgs.msg import Float32

''' Parameter '''
# Set the odom frame
odom_frame = '/odom'
base_frame = '/base_footprint'
rate = 20 # ROS rate
angular_speed = 0.2
angular_tolerance = radians(2.5)
published_topic = 'cmd_vel_mux/input/switch'
rospy.init_node('turn_turtlebot', anonymous=False)
cmd_vel = rospy.Publisher(published_topic, Twist, queue_size=10)
tf_listener = tf.TransformListener() # Initialize the tf listener
r = rospy.Rate(rate)

def control_angle():


    rospy.Subscriber("chatter", Float32, input_angle) #change to custom topic


    # Find out if the robot uses /base_link or /base_footprint
    try:
        tf_listener.waitForTransform(odom_frame, '/base_footprint', rospy.Time(), rospy.Duration(1.0))
        base_frame = '/base_footprint'
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        try:
            tf_listener.waitForTransform(odom_frame, '/base_link', rospy.Time(), rospy.Duration(1.0))
            base_frame = '/base_link'
        except (tf.Exception, tf.ConnectivityException, tf.LookupException):
            rospy.loginfo("Cannot find transform between /odom and /base_link or /base_footprint")
            rospy.signal_shutdown("tf Exception")

    rospy.spin()

def get_odom():
    # Get the current transform between the odom and base frames
    try:
        (trans, rot)  = tf_listener.lookupTransform(odom_frame, base_frame, rospy.Time(0))
    except (tf.Exception, tf.ConnectivityException, tf.LookupException):
        rospy.loginfo("TF Exception")
        return

    return (Point(*trans), quat_to_angle(Quaternion(*rot)))

def input_angle(goal_angle):
    # Initialize the position variable as a Point type
    position = Point()

    # get current orientation
    (position, rotation) = get_odom()
    rot_start = rotation

    # Loop once for each leg of the trip
    for i in range(2):
        # Initialize the movement command
        move_cmd = Twist()
        cmd_vel.publish(move_cmd)
        rospy.sleep(1)

        # Track how far we have turned
        turn_angle = 0

        while abs(delta_angle) < abs(goal_angle):
            # Publish the Twist message and sleep 1 cycle
            cmd_vel.publish(move_cmd)
            r.sleep()

            # Get the current rotation
            (pos, rot) = get_odom()

            # Compute the amount of rotation since the last loop
            delta_angle = normalize_angle(rot_start - rot)
            move_cmd.angular.z = angular_speed

        # Stop the robot before the next leg
        move_cmd = Twist()
        cmd_vel.publish(move_cmd)
        rospy.sleep(1)

    # Stop the robot for good
    cmd_vel.publish(Twist())

def normalize_angle(angle):
    res = angle
    while res > pi:
        res -= 2.0 * pi
    while res < -pi:
        res += 2.0 * pi
    return res

def quat_to_angle(quat):
    rot = PyKDL.Rotation.Quaternion(quat.x, quat.y, quat.z, quat.w)
    return rot.GetRPY()[2]


if __name__ == '__main__':
    control_angle()
